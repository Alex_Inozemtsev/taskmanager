package ru.mephi.taskmanager.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
public class Comment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String content;

    private Timestamp createdAt;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "createdBy")
    private User createdBy;
}

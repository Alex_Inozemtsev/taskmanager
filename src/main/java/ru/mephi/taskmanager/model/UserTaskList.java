package ru.mephi.taskmanager.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "user_list")
@IdClass(UserTaskListID.class)
public class UserTaskList {

    @Id
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @Id
    @ManyToOne
    @JoinColumn(name = "list_id", referencedColumnName = "id")
    private TaskList list;


    private SharingType type;

}

package ru.mephi.taskmanager.model;



public enum Status {
    NOT_STARTED,
    IN_PROCESS,
    DONE
}

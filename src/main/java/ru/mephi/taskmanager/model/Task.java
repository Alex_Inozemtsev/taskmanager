package ru.mephi.taskmanager.model;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cascade;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Getter
@Setter
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany
    @JoinColumn(name = "task_id")
    private Set<Comment> comments;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "createdBy")
    private User createdBy;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "updatedBy")
    private User updatedBy;

    @Column(nullable = false)
    private String title;
    private String description;
    private Status status;

    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Timestamp startTime;
    private Timestamp endTime;

}

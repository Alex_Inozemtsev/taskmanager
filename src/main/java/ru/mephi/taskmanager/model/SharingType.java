package ru.mephi.taskmanager.model;

public enum SharingType {
    OWNER,
    EDITOR,
    COMMENTATOR,
    VIEWER
}

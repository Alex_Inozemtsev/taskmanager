package ru.mephi.taskmanager.model;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;

@Entity
@Getter
@Setter
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(nullable = false)//??
    private Long id;

    private String fullName;
    private String userName;
    private String password;
    private Timestamp lastLogin;

    private String email;

    @OneToMany(mappedBy = "list")
    private Set<UserTaskList> availableLists;

}

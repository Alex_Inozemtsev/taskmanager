package ru.mephi.taskmanager.model;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Map;
import java.util.Set;

@Getter
@Entity
@Setter
public class TaskList {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany
    @JoinColumn(name = "list_id")
    private Set<Task> tasks;

    @ManyToMany(mappedBy = "lists", fetch = FetchType.EAGER)
    private Set<Tag> tags;

    @OneToMany(mappedBy = "user")
    private Set<UserTaskList> users;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "createdBy")
    private User createdBy;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "updatedBy")
    private User updatedBy;

    @Column(nullable = false)
    private String title;
    private String description;
    private Status status;

    private Timestamp createdAt;
    private Timestamp updatedAt;
    private Timestamp startTime;
    private Timestamp endTime;
}
